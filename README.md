# Mise en place de l'environnement 

- Se connecter à [cloud-info.univ-lyon1.fr](https://cloud-info.univ-lyon1;fr)
- Ajouter de sa clé publique dans l'onglet _Key Pairs_

## Préparation de la VM Master

- Créer d'une instance avec l'image _Ubuntu Server 22.04.3 LTS - Docker Ready_ avec _m1.medium_ (4Go de RAM et 2 CPU), ajouter sa clé publique dans _Key Pair_

- Se connecter en SSH sur l'instance avec l'utilisateur Ubuntu : 
	`ssh ubuntu@192.168.xx.xx` (l'IP est visible dans l'affichage de l'instance une fois que celle-ci est créée)

- créer une clé SSH qui permettra de se connecter aux nodes
```bash
ssh-keygen -t rsa
```

## Préparation des workers 

- Créer deux instances _Ubuntu Server 22.04.3 LTS - Docker Ready_ avec _m1.smedium_ (4Go de RAM et 1 CPU), ajouter sa clé publique dans _Key Pair_

- copier le contenu du fichier `/home/ubuntu/.ssh/idea_rsa.pub` **du master** dans le fichier `/home/ubuntu/.ssh/authorized_keys` **sur les workers et sur le master** (pour qu'il puisse se SSH lui-même) 

- tester la connexion SSH aux 3 machines depuis le **master**

# Mise en place du cluster Kubernetes avec RKE

## Mise en place du cluster kube avec RKE 

- Installation de RKE 
```bash
wget https://github.com/rancher/rke/releases/download/v1.5.3/rke_linux-amd64
sudo mv rke_linux-amd64 /usr/local/bin/rke
sudo chmod +x /usr/local/bin/rke
rke --version
```


- utiliser la commande `rke config` et répondre aux questions
	- Le cluster doit avoir 3 noeuds (hosts)
	- Le noeud master a le rôle Control Plane et etcd
	- Les noeuds workers ont uniquement le rôle worker

- lancer `rke up` pour déployer le cluster

> [!NOTE]
> 
> Pour ne plus être gêné par le par-feu, il faut ajouter au fichier `/etc/environment` la variable d'environnement suivante : 
> ```
> NO_PROXY=univ-lyon1.fr,127.0.0.1,localhost,192.168.0.0/16 
> ```

# Installation du Cloud Controller Manager du Cloud Provider

- Créer l'_Application Credential_ dans OpenStack qui permettra à RKE de configurer le Cloud Provider. 
  Pour cela, sur Horizon : _Identity -> Application Credentials -> Create_. Une fois l'application credential créé, une popup s'affiche. Il faut cliquer sur _cloud.yaml_ et le télécharger. Dans ce fichier, il y a toutes les informations nécessaires à la configuration du cloud provider. 

- Créer le fichier `/etc/kubernetes/cloud.conf`
```  
[Global]           
auth-url=https://cloud-info.univ-lyon1.fr:5000/v3
application_credential_id=
application_credential_secret=     
region=RegionOne
tenant-id=
```
Les valeurs de `application_credentail_id` et `application_credential.secret` sont dans le fichier _cloud.yaml_ téléchargé à l'étape précédente. 
Le `tenant-id` correspond au Project ID, qu'on trouve sur Horizon dans : _Identity -> Projects_

- Créer un objet _Secret_ dans Kubernetes avec les informations de connexion du projet Openstack, dans le namespace `kube-system`
```bash
kubectl create secret -n kube-system generic cloud-config --from-file=/etc/kubernetes/cloud.conf
```

- Créer les ressources RBAC et le DaemonSet du Cloud Controller manager
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/cloud-controller-manager-roles.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/cloud-controller-manager-role-bindings.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/cloud-provider-openstack/master/manifests/controller-manager/openstack-cloud-controller-manager-ds.yaml
```

- Lancer la commande 
```bash
kubectl get pods -A
```
Une fois que tous les pods ont le statut _running_, le Cloud Provider a été correctement mis en place. (Cela peut prendre quelques minutes)

# Mise en place du plugin Cinder 

- Créer un fichier de valeurs personnalisées dans `csi-cinder-plugin/values.yaml`
```bash
secret:
  enabled: true
  create: false
  name: cloud-config
```
- Installer le Helm Chart : 
```bash
helm repo add cpo https://kubernetes.github.io/cloud-provider-openstack
helm repo update
helm install -n kube-system cinder-csi cpo/openstack-cinder-csi
```

- La commande `kubectl -n kube-system get sc` doit avoir pour résultat quelque chose comme : 
```bash
csi-cinder-sc-delete
csi-cinder-sc-retain
```

# Déploiement de Nextcloud

- Créer le namespace `nextcloud` : 
```bash
kubectl create namespace nextcloud
```
- Créer un fichier de valeurs personnalisées `nextcloud/values.yaml` : 
```yaml
persistence:
  enabled: true
  storageClass: csi-cinder-cs-delete

ingress:
  enabled: true
  className: nginx
```
- Installer le Helm Chart : 
```bash
helm repo add nextcloud https://nextcloud.github.io/helm/
helm repo update
helm install -n nextcloud nextcloud nextcloud/nextcloud --values nextcloud/values.yaml
```

